<?php

class AmoTasks extends AmoElements
{
    public const AMO_TASK_ELEMENT_CONTACT = 1;
    public const AMO_TASK_ELEMENT_LEAD = 2;
    public const AMO_TASK_ELEMENT_COMPANY = 3;
    public const AMO_TASK_ELEMENT_CONSUMER = 4;

    public const AMO_TASK_TYPE_CALL = 1;
    public const AMO_TASK_TYPE_MEETING = 2;
    public const AMO_TASK_TYPE_EMAIL = 3;

    static protected $element = Amo::AMO_ELEMENT_TASKS;
    // Endpoint для api задач
    static protected $url = '/api/v2/' . Amo::AMO_ELEMENT_TASKS;

    /**
     * Создание задачи
     *
     * @param string $text - Текст задачи
     * @param int $elementId - Id улемента
     * @param int $elementType - Тип елемента amocrm для задачи
     * @param int $taskType - Тип задачи
     * @param int  $completeAt - Timestamp окончания задачи
     * @param int $responsibleUserId - Id пользователя ответственного за задачу
     * @return int|null
     */
    public function add($text, $elementId, $elementType, $taskType, $completeAt, $responsibleUserId)
    {
        $data = [
            'add' => [
                [
                    'element_id' => $elementId,
                    'element_type' => $elementType,
                    'complete_till_at' => $completeAt,
                    'task_type' => $taskType,
                    'text' => $text,
                    'responsible_user_id' => $responsibleUserId
                ]
            ]
        ];

        $response = $this->amo::postRequest(static::$url, $data);

        if ($this->checkResponse($response)) {
            return $response['response'][0]['id'] ?? null;
        }

        return null;
    }
}