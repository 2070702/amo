<?php

class AmoAccount extends AmoElements
{
    // Вернёт информацию по всем дополнительным полям в аккаунте
    public const AMO_WITH_CUSTOM_FIELDS = 'custom_fields';
    // Вернёт информацию по всем пользователям в аккаунте
    public const AMO_WITH_USERS = 'users';
    // Вернёт информацию по всем цифровым воронкам в аккаунте
    public const AMO_WITH_PIPELINES = 'pipelines';
    // Вернёт информацию по всем группам пользователей в аккаунте
    public const AMO_WITH_GROUPS = 'groups';
    // Вернёт информацию по всем типам дополнительных полей в аккаунте
    public const AMO_WITH_NOTE_TYPES = 'note_types';
    // Вернёт информацию по всем типам задач в аккаунте
    public const AMO_WITH_TASK_TYPES = 'task_types';

    /** @var array|null Параметры аккаунта */
    private $account;

    static protected $element = Amo::AMO_ELEMENT_ACCOUNT;
    // Endpoint для api аккаунта
    static protected $url = '/api/v2/' . Amo::AMO_ELEMENT_ACCOUNT;

    public function __construct(Amo $amo)
    {
        parent::__construct($amo);
        if (null !== ($account = $this->read())) {
            $this->account = $account;
        }
    }

    public function read($withs = [AmoAccount::AMO_WITH_CUSTOM_FIELDS, AmoAccount::AMO_WITH_USERS, AmoAccount::AMO_WITH_PIPELINES, AmoAccount::AMO_WITH_GROUPS, AmoAccount::AMO_WITH_NOTE_TYPES, AmoAccount::AMO_WITH_TASK_TYPES])
    {
        if (!$this->amo->isAuth()) {
            return null;
        }

        return false !== ($response = $this->amo::getRequest(self::$url, 0 !== count($withs) ? ['with' => implode(',', $withs)] : [])) && $this->checkResponse($response) ? $response['response'] : null;
    }

    public function getDatePattern($name)
    {
        return $this->account['date_pattern'][$name] ?? null;
    }

    public function getValue($name)
    {
        return $this->account[$name] ?? null;
    }

    public function getFieldId($entity, $code)
    {
        if (!isset($this->account['_embedded']['custom_fields'][$entity])) {
            return null;
        }

        $code = strtolower(trim($code));

        foreach ($this->account['_embedded']['custom_fields'][$entity] as $field) {
            if ($code === strtolower(trim($field['code']))) {
                return $field['id'];
            }
        }

        return null;
    }

    public function getManagers()
    {
        if (!isset($this->account['_embedded']['users'])) {
            return null;
        }

        $managers = [];

        foreach ($this->account['_embedded']['users'] as $id => $user) {
            if ($user['is_active'] && !$user['is_admin']) {
                $managers[$id] = $user;
            }
        }

        return count($managers) !== 0 ? $managers : null;
    }

    public function getAdmin()
    {
        if (!isset($this->account['_embedded']['users'])) {
            return null;
        }

        foreach ($this->account['_embedded']['users'] as $id => $user) {
            if ($user['is_admin']) {
                return $user;
            }
        }

        return null;
    }


    public function getMainPipelineId()
    {
        if (!isset($this->account['_embedded']['pipelines'])) {
            return null;
        }

        foreach ($this->account['_embedded']['pipelines'] as $id => $pipeline) {
            if ($pipeline['is_main']) {
                return $id;
            }
        }

        return null;
    }

    public function getPipelineDefaultStatusId($pipelineId)
    {
        if (!isset($this->account['_embedded']['pipelines'][$pipelineId])) {
            return null;
        }

        $statuses = array_keys($this->account['_embedded']['pipelines'][$pipelineId]['statuses']);

        return $this->account['_embedded']['pipelines'][$pipelineId]['statuses'][$statuses[0]]['id'];
    }
}