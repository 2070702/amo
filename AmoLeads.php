<?php

class AmoLeads extends AmoElements
{
    static protected $element = Amo::AMO_ELEMENT_LEADS;
    // Endpoint для api сделок
    static protected $url = '/api/v2/' . Amo::AMO_ELEMENT_LEADS;

    /**
     * Создание сделки
     *
     * @param string $name - Название сделки
     * @param int $pipelineId - Id воронки продаж
     * @param int $statusId - Id этапа воронки продаж
     * @param int $responsibleUserId - Id пользователя ответственного за сделку
     * @param int[] $contacts - масив Id контактов
     * @return int | null
     */
    public function add($name, $pipelineId, $statusId, $responsibleUserId, $contacts)
    {
        $data = [
            'add' => [
                [
                    'name' => $name,
                    'responsible_user_id' => $responsibleUserId,
                    'pipeline_id' => $pipelineId,
                    'status_id' => $statusId,
                    'contacts_id' => $contacts
                ]
            ]
        ];

        $response = $this->amo::postRequest(static::$url, $data);

        if ($this->checkResponse($response)) {
            return $response['response'][0]['id'] ?? null;
        }

        return null;
    }
}