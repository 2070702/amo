<?php

include_once 'Amo.php';

try {
    // Проверяем метод запроса к fpi
    if (isset($_SERVER['REQUEST_METHOD']) && 'POST' !== $_SERVER['REQUEST_METHOD']) {
        throw new RuntimeException('Неверный метод запроса');
    }

    // Читаем входящие данные
    $params = json_decode(file_get_contents('php://input'), true);

    // Проверка поступивших данных
    if (!$params || !is_array($params)) {
        throw new RuntimeException('Нет данных в запросе');
    }

    // Проверка поля E-Mail
    if (!isset($params['email']) || '' === trim($params['email'] || !filter_var($params['email'], FILTER_VALIDATE_EMAIL))) {
        throw new RuntimeException('Не верно заполнено поле "E-mail"');
    }

    // Проверка поля телефона
    if (!isset($params['phone']) || '' === trim($params['phone'] || !validate_phone_number($params['phone']))) {
        throw new RuntimeException('Не верно заполнено поле "Телефон"');
    }

    // Проверка поля названия
    if (!isset($params['name'])) {
        $params['name'] = '';
    } else {
        $params['name'] = trim($params['name']);
    }

    // Авторизация через API при ошибке RuntimeException
    $amo = new Amo('amo2070702com', 'amo@2070702.com', '8607b5c3132c772b99ffe57aa437cd41b947430b');


    // Поиск контакта по адресу электронной почты результат масив с единственным элементом, при ошибке RuntimeException
    if (null !== ($contact = $amo->contacts->find($params['email'])) && is_array($contact)) {
        $contact = $contact[0];
    }

    // Поиск контакта по номеру телефона, если не найден по email, результат масив с единственным элементом, при ошибке RuntimeException
    if (null === $contact && null !== ($contact = $amo->contacts->find($params['phone'])) && is_array($contact)) {
        $contact = $contact[0];
    }

    if (null === $contact) {
        // Создание контакта

        // Распределение
        // Список пользователей CRM исключая администратора
        $managers = $amo->account->getManagers();

        // Создаем масив для учета количества сделок пользователей
        $managerLeadsCount = [];
        foreach ($managers as $id => $data) {
            $managerLeadsCount[$id] = [];
        }

        // Поиск сделок созданых в течении дня
        $leads = $amo->leads->find('', [
            'filter' => [
                'date_create' => [
                    // Текущая дата
                    'from' => date_create(date('Y-m-d'))
                        ->setTimezone(new DateTimeZone($amo->account->getValue('timezone')))
                        ->format($amo->account->getDatePattern('date')),
                    // Дата следующего дня
                    'to' => date_create(date('Y-m-d'))
                        ->setTimezone(new DateTimeZone($amo->account->getValue('timezone')))
                        ->add(new DateInterval('P1D'))
                        ->format($amo->account->getDatePattern('date'))
                ]
            ]
        ]);

        if (null !== $leads) {
            // Перебор сделок
            foreach ($leads as $lead) {
                // Если нет пользователя в массиве учета количества сделок то добовляем его
                if (!isset($managers[$lead['responsible_user_id']])) {
                    $managerLeadsCount[$lead['responsible_user_id']] = [];
                }

                // Прверка есть ли указанный контакт в масиве учета сделок
                if (isset($lead['main_contact']['id']) && !isset($managerLeadsCount[$lead['responsible_user_id']][$lead['main_contact']['id']])) {
                    $managerLeadsCount[$lead['responsible_user_id']][$lead['main_contact']['id']] = $lead['id'];
                }
            }
        }

        // Выбор первого пользователя с минимальным количеством сделок
        $responsibleUserId = null;
        $leadsCount = 0;
        foreach ($managerLeadsCount as $id => $list) {
            if (!$responsibleUserId) {
                $responsibleUserId = $id;
                $leadsCount = count($list);
            } elseif ($leadsCount > count($list)) {
                $responsibleUserId = $id;
                $leadsCount = count($list);
            }
        }

        // Создание контакта, результат id контакта, при ошибке RuntimeException
        $contactId = $amo->contacts->add($params['name'], $params['email'], $params['phone'], $responsibleUserId);
    } else {
        $contactId = $contact['id'];
        $responsibleUserId = $contact['responsible_user_id'];
    }

    // Id воронки продаж по умолчанию, при ошибке RuntimeException
    $pipelineId = $amo->account->getMainPipelineId();
    // Id первого статуса в воронке продаж, при ошибке RuntimeException
    $statusId = $amo->account->getPipelineDefaultStatusId($pipelineId);

    // Создание сделки, результат id сделки, при ошибке RuntimeException
    $leadId = $amo->leads->add(
        'Заявка с сайта',
        $pipelineId,
        $statusId,
        $responsibleUserId,
        [$contactId]
    );

    // Дата окончания задачи. Если до 15 часов то ставим предел 23:59 текущего дня
    // иначе ставим предел до 23:59 следующего дня
    $completeAt = date_create();

    if ((int)date('H') < 15) {
        $completeAt->setTime(23, 59);
    } else {
        $completeAt->add(new DateInterval('P1D'))->setTime(23, 59);
    }

    // Создание задачи прикрепленной к сделке, при ошибке RuntimeException
    $amo->tasks->add(
        'Перезвонить клиенту',
        $leadId,
        AmoTasks::AMO_TASK_ELEMENT_LEAD,
        AmoTasks::AMO_TASK_TYPE_CALL,
        $completeAt->getTimestamp(),
        $responsibleUserId
    );

    // Сообщение об создании контакта
    header('Content-Type: application/json');

    echo json_encode([
        'success' => true,
        'message' => 'Заявка создана'
    ]);

    // Отправка письма администратору
    if (null !== ($admin = $amo->account->getAdmin())) {
        @mail($admin['login'], 'Заявка с сайта', 'name: ' . $params['name'] . PHP_EOL . ' email: ' . $params['email'] . PHP_EOL . 'phone: ' . $params['phone']);
    }

} catch (RuntimeException $e) {
    // Сообщение об ошибке

    header('HTTP/1.0 400 Bad request');
    header('Content-Type: application/json');

    echo json_encode([
        'success' => false,
        'error' => $e->getMessage()
    ]);

}

function validate_phone_number($phone)
{
    // Доступны символы +, - и . в телефоном номере
    $filteredPhoneNumber = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
    // Удаляем символ "-" с номера
    $phoneToCheck = str_replace('-', '', $filteredPhoneNumber);
    // Проверка на количество символов в номере
    return !(strlen($phoneToCheck) < 10 || strlen($phoneToCheck) > 14);
}