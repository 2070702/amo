<?php

class AmoContacts extends AmoElements
{
    static protected $element = Amo::AMO_ELEMENT_CONTACTS;
    // Endpoint для api контактов
    static protected $url = '/api/v2/' . Amo::AMO_ELEMENT_CONTACTS;

    /**
     * Добавление контакта
     *
     * @param string $name - Название контакта
     * @param string $email - Адрес электронной почты
     * @param string $phone - Номер телефона
     * @param int $responsibleUserId - Id пользователя ответственного за контакт
     * @return int|null
     */
    public function add($name, $email, $phone, $responsibleUserId)
    {
        $data = [
            'add' => [
                [
                    'name' => $name,
                    'responsible_user_id' => $responsibleUserId,
                    'custom_fields' => [
                        [
                            'id' => $this->amo->account->getFieldId(static::$element, 'EMAIL'),
                            'values' => [
                                [
                                    'value' => $email,
                                    'enum' => 'WORK'
                                ]
                            ]
                        ],
                        [
                            'id' => $this->amo->account->getFieldId(static::$element, 'PHONE'),
                            'values' => [
                                [
                                    'value' => $phone,
                                    'enum' => 'WORK'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $response = $this->amo::postRequest(static::$url, $data);

        if ($this->checkResponse($response)) {
            return $response['response'][0]['id'] ?? null;
        }

        return null;
    }
}