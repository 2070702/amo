<?php
/** @noinspection PhpUnused */
/** @noinspection MagicMethodsValidityInspection */

include_once 'AmoError.php';
include_once 'AmoElements.php';
include_once 'AmoAccount.php';
include_once 'AmoContacts.php';
include_once 'AmoLeads.php';
include_once 'AmoTasks.php';

/**
 * @property AmoAccount account
 * @property AmoContacts contacts
 * @property AmoLeads leads
 * @property AmoTasks tasks
 */
class Amo
{
    public const AMO_ELEMENT_AUTH = 'auth';
    public const AMO_ELEMENT_CONTACTS = 'contacts';
    public const AMO_ELEMENT_LEADS = 'leads';
    public const AMO_ELEMENT_COMPANIES = 'companies';
    public const AMO_ELEMENT_TASKS = 'tasks';
    public const AMO_ELEMENT_ACCOUNT = 'account';

    public const HTTP_GET = 'GET';
    public const HTTP_POST = 'POST';

    private $login;
    private $hash;
    private $auth = false;

    static private $subdomain;
    static private $cookieFileName = __DIR__ . '/.cookie';
    static private $userAgent = 'amocrm-client 1.0';
    static private $instances = [];

    static private $httpErrors = [
        301 => 'Moved permanently',
        400 => 'Bad request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not found',
        500 => 'Internal server error',
        502 => 'Bad gateway',
        503 => 'Service unavailable'
    ];

    public function __construct($subdomain, $login, $hash)
    {
        static::$subdomain = $subdomain;
        $this->login = $login;
        $this->hash = $hash;

        $this->auth();
    }

    public function __get($name)
    {
        $className = 'Amo' . str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));

        if (!class_exists($className)) {
            throw new RuntimeException('Метод не существует: ' . $name);
        }

        if (!isset(static::$instances[$className])) {
            static::$instances[$className] = new $className($this);
        }

        return self::$instances[$className];
    }

    /**
     * @return bool
     */
    public function isAuth()
    {
        return $this->auth;
    }

    /**
     * @return bool
     */
    public function auth()
    {
        if ('' === trim($this->login)) {
            AmoError::error('Не указан логин пользователя');
        }

        if ('' === trim($this->hash)) {
            AmoError::error('Не указан ключ пользователя');
        }

        if (false !== ($response = static::postRequest('/private/api/auth.php?type=json', ['USER_LOGIN' => $this->login, 'USER_HASH' => $this->hash]))) {
            // Флаг авторизации доступен в свойстве "auth"
            if ($response['success'] && isset($response['response']['auth']) && $response['response']['auth']) {
                $this->auth = true;
                return true;
            }

            if (!$response['success']) {
                AmoError::errorResponse($response, static::AMO_ELEMENT_AUTH);
            }
        }

        AmoError::error('Авторизация не удалась');

        return false;
    }

    private static function _curl($method, $url, $params = [])
    {
        $link = 'https://' . static::$subdomain . '.amocrm.ru' . $url . ($method === static::HTTP_GET && 0 !== count($params) ? '?' . http_build_query($params) : '');

        $curl = curl_init(); // Сохраняем дескриптор сеанса cURL

        // Устанавливаем необходимые опции для сеанса cURL

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, self::$userAgent);
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, self::$cookieFileName);
        curl_setopt($curl, CURLOPT_COOKIEJAR, self::$cookieFileName);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);

        if (static::HTTP_POST === $method) {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        }

        $out = curl_exec($curl); // Инициируем запрос к API и сохраняем ответ в переменную

        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE); // Получим HTTP-код ответа сервера

        curl_close($curl); // Завершаем сеанс cURL

        $code = (int)$code;

        try {
            // Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
            if ($code !== 200 && $code !== 204) {
                $response = null;
                if ($out && '' !== trim($out)) {
                    $response = json_decode($out, true);
                }

                return [
                    'success' => false,
                    'message' => self::$httpErrors[$code] ?? 'Неизвестная ошибка ' . $code,
                    'response' => $response['response'] ?? null
                ];
            }
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage() . PHP_EOL . 'Код ошибки: ' . $e->getCode()
            ];
        }

        // Данные получаем в формате JSON
        $response = json_decode($out, true);

        if ($code === 204) {
            return [
                'success' => true,
                'response' => $response
            ];
        }

        if (isset($response['response'])) {
            return [
                'success' => true,
                'response' => $response['response']
            ];
        }

        if (isset($response['_embedded']['errors'])) {
            return [
                'success' => false,
                'message' => $response['_embedded']['errors']
            ];
        }

        if (isset($response['_embedded']['items'])) {
            return [
                'success' => true,
                'response' => $response['_embedded']['items']
            ];
        }

        if ($code === 200) {
            return [
                'success' => true,
                'response' => $response
            ];
        }

        return [
            'success' => false,
            'message' => 'Неизвестная ошибка'
        ];
    }

    /**
     * @param string $url
     * @param array $data
     *
     * @return array
     */
    public static function postRequest($url, $data = [])
    {
        return static::_curl(static::HTTP_POST, $url, $data);
    }


    /**
     * @param string $url
     * @param array $params
     *
     * @return array
     */
    public static function getRequest($url, $params = [])
    {
        return static::_curl(static::HTTP_GET, $url, $params);
    }
}