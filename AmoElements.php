<?php

abstract class AmoElements
{
    protected $amo;
    static protected $element;
    static protected $url;

    public function __construct(Amo $amo)
    {
        $this->amo = $amo;
    }

    /**
     * @param $response
     * @param $element
     *
     * @return bool
     */
    protected function checkResponse($response, $element = null)
    {
        if ($response['success']) {
            return true;
        }

        if (null === $element) {
            $element = static::$element;
        }

        if (isset($response['response']['error_code'])) {
            AmoError::errorResponse($response, $element);
        }

        if (isset($response['message'])) {
            AmoError::error($response['message']);
        }

        return false;
    }


    /**
     * @param string $query
     * @param array $params
     * @return array|null
     */
    public function find($query, $params = [])
    {
        if (!$this->amo->isAuth()) {
            return null;
        }

        if ('' !== trim($query)) {
            $params = array_merge(['query' => $query], $params);
        }

        if (false !== ($response = $this->amo::getRequest(static::$url, $params)) && $this->checkResponse($response)) {
            return $response['response'] ?? null;
        }

        return null;
    }
}